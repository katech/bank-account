Feature: In order to retrieve some or all of my savings
  As a bank client
  I want to make a withdrawal from my account

  Background:
    Given i have a bank account with balance 500

  Scenario: i want to retrieve some of my saving;
    When i am withdrawing 250 from my account
    Then my balance must be 250
