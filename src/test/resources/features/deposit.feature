Feature: In order to save money
    As a bank client
    I want to make a deposit in my account

    Background:
        Given i have a bank account with balance 0


    Scenario: i want to make deposit in my account;
        When i am depositing 200 in my account
        Then my balance must be 200
