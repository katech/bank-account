package com.socgen.kata.bankaccount.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class BalanceTest {

    private Account account;

    @BeforeEach
    public void init() {
        account = new Account(Clock.systemDefaultZone());

    }

    @Test
    public void test_getBalance_for_an_account_without_operations_yet() {
        final Balance actualBalance = account.getBalance();
        final Balance expectedBalance = new Balance(0L);
        assertEquals(expectedBalance, actualBalance);
    }

}
