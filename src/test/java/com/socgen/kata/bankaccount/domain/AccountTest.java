package com.socgen.kata.bankaccount.domain;

import com.socgen.kata.bankaccount.util.EOperationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

    private Account account;


    @Mock
    private StatementHistoryPrinter statementHistoryPrinter;

    @BeforeEach
    public void init() {
        account = new Account(Clock.systemDefaultZone());
    }

    @Captor
    private ArgumentCaptor<StatementHistoryEntry> entryArgumentCapture;

    @Test
    public void should_deposit_200euros_with_success() {
        account.deposit(new Amount(200));
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_deposit_2000euros_and_5500euros_with_success() {
        account.deposit(new Amount(2000));
        account.deposit(new Amount(5500));
        final Balance expectedBalance = new Balance(7500L);
        assertEquals(expectedBalance, account.getBalance());
    }
    @Test
    public void should_Withdrawal_300euros_with_success() {
        account.deposit(new Amount(500));
        account.withdraw(new Amount(300));
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_Withdrawal_with_100euros_and_200euros_with_success() {
        account.deposit(new Amount(500));
        account.withdraw(new Amount(100));
        account.withdraw(new Amount(200));
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_writeTo_when_two_entries_are_expected() {
        account.deposit(new Amount(5000));
        account.withdraw(new Amount(2000));
        account.printHistory(statementHistoryPrinter);
        Mockito.verify(statementHistoryPrinter, times(2)).print(ArgumentMatchers.any(StatementHistoryEntry.class));
    }

    @Test
    public void should_writeTo_when_one_entry_is_expected() {
        account.deposit(new Amount(5000));
        account.printHistory(statementHistoryPrinter);
        Mockito.verify(statementHistoryPrinter, times(1)).print(entryArgumentCapture.capture());
        final StatementHistoryEntry actualStatementHistoryEntry = entryArgumentCapture.getValue();
        assertNotNull(actualStatementHistoryEntry);
        assertEquals(EOperationType.DEPOSIT, actualStatementHistoryEntry.operationType());
        assertNotNull(actualStatementHistoryEntry.operationDate());
        assertEquals(new Amount(5000), actualStatementHistoryEntry.amount());
        assertEquals(new Balance(5000), actualStatementHistoryEntry.balance());
    }

    @Test
    public void should_writeTo_when_no_entry_found() {
        account.printHistory(statementHistoryPrinter);
        Mockito.verifyNoMoreInteractions(statementHistoryPrinter);
    }


}
