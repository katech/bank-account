package com.socgen.kata.bankaccount.bdd;

import com.socgen.kata.bankaccount.domain.Account;
import com.socgen.kata.bankaccount.domain.Amount;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertThat;

public class DepositSteps {

    private Account account;
    @Given("^i have a bank account with balance (\\d+)$")
    public void i_have_a_bank_account_with_balance(Integer balance) throws Throwable {
        //this.account = new Account(new Amount(balance));
    }

    @When("^i am depositing (-?\\d+) in my account$")
    public void i_deposit(Integer amount) {
        try {
          // this.account.deposit(new Amount(amount));
        }catch(IllegalArgumentException e) {
            //DO Nothing (this is what we are waiting for)
        }
    }

    @Then("^my balance must be (\\d+)$")
    public void my_balance_must_be(Integer amount) throws Throwable {
       // assertThat(this.account.currentBalance()).isEqualTo(new Amount(amount));
    }
}
