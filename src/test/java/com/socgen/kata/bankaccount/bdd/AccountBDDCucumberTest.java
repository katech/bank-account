package com.socgen.kata.bankaccount.bdd;

import com.socgen.kata.bankaccount.domain.Account;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AccountBDDCucumberTest  {

	private String idAccount = "";
	private double amount = 0;
	private double initBalance = 200;
	private Account account;

	@Given("^As a bank client \"([^\"]*)\"$")
	public void initiateAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	@When("^I want to make a deposit of \"([^\"]*)\" in my account$")
	public void deposit(double amount) throws Exception {
		this.amount = amount;

	}



	@Then("^the deposit is done$")
	public void validateDeposit() throws Exception {

	}

}
