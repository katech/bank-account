package com.socgen.kata.bankaccount.domain;

import com.socgen.kata.bankaccount.util.EOperationType;

public record Balance (double value){

    /**
     * @param operationType
     * @param amount
     * @return
     */
    public Balance update(EOperationType operationType, Amount amount) {
        if (EOperationType.DEPOSIT.equals(operationType)) {
            return new Balance(value + amount.value());
        } else if (EOperationType.WITHDRAWAL.equals(operationType)) {
            return new Balance(value - amount.value());
        }
        throw new IllegalArgumentException(operationType + " isn't a supported operation.");
    }

}
