package com.socgen.kata.bankaccount.domain;

@FunctionalInterface
public interface IAccountWithDraw {

    void withdraw(Amount amount);
}
