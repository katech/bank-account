package com.socgen.kata.bankaccount.domain;

public record Amount (double value){}
