package com.socgen.kata.bankaccount.domain;

/**
 * @author mohamedmassmoudi
 *
 */
public interface StatementHistoryPrinter {
	void print(StatementHistoryEntry statementHistoryEntry);

}
