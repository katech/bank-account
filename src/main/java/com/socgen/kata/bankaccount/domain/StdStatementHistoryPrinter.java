package com.socgen.kata.bankaccount.domain;

import java.util.StringJoiner;

public class StdStatementHistoryPrinter implements StatementHistoryPrinter{

	
	@Override
	public void print(StatementHistoryEntry statementHistoryEntry) {
	     System.out.println(formatStatementHistoryEntry(statementHistoryEntry));
	}

	/**
	 * @param statementHistoryEntry
	 * @return
	 */
	private static String formatStatementHistoryEntry(StatementHistoryEntry statementHistoryEntry) {
		return new StringJoiner(" | ").add("Operation='" + statementHistoryEntry.operationType() + "'")
				.add("Date='" + statementHistoryEntry.operationDate() + "'")
				.add("Amount='" + statementHistoryEntry.amount().value() + "'")
				.add("Balance='" + statementHistoryEntry.balance().value() + "'").toString();
	}
}
