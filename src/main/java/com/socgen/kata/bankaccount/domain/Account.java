package com.socgen.kata.bankaccount.domain;

import com.socgen.kata.bankaccount.util.EOperationType;

import java.time.Clock;
import java.time.Instant;

public class Account implements IAccountDeposit, IAccountWithDraw{

    private final Clock clock;

    private final StatementsHistory history = new StatementsHistory();

    private Balance balance = new Balance(0);

    /**
     * @param clock
     */
    public Account(Clock clock) {
        super();
        this.clock = clock;
    }

    public Balance getBalance(){
        return this.balance;
    }

    @Override
    public void deposit(Amount amount) {
        balance = balance.update(EOperationType.DEPOSIT, amount);
        addToHistory(EOperationType.DEPOSIT, clock.instant(), amount, balance);
    }


    @Override
    public void withdraw(Amount amount) {
        balance = balance.update(EOperationType.WITHDRAWAL, amount);
        addToHistory(EOperationType.WITHDRAWAL, clock.instant(), amount, balance);
    }

    public void printHistory(StatementHistoryPrinter statementHistoryPrinter) {
        history.print(statementHistoryPrinter);
    }

    private void addToHistory(EOperationType operationType, Instant operationDate, Amount amount, Balance balance) {
        history.addEntry(operationType, operationDate, amount, balance);
    }

}
