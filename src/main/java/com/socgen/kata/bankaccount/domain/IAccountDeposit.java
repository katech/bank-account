package com.socgen.kata.bankaccount.domain;

@FunctionalInterface
public interface IAccountDeposit {
    void deposit(Amount amount);

}
