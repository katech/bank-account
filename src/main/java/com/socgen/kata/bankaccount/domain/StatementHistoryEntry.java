package com.socgen.kata.bankaccount.domain;

import com.socgen.kata.bankaccount.util.EOperationType;

import java.time.Instant;

public record StatementHistoryEntry(EOperationType operationType, Instant operationDate, Amount amount, Balance balance) {}
