package com.socgen.kata.bankaccount.util;

public enum EOperationType {
    WITHDRAWAL, DEPOSIT
}
